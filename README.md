# Block Extra

- Provides an extra functionality not found in core related to content blocks and plugin blocks.

## Table of contents

- Introduction
- Requirements
- Installation
- Configuration
- Maintainers

## Introduction

- Provides an extra functionality not found in core related to content blocks and plugin blocks.
- Provides an edit button and a preview on '/admin/structure/block/manage/BLOCK_ID'.

## Requirements

- This module requires no modules outside of Drupal core.

## Installation

- Install this module as you would normally install
  a contributed Drupal module.
  [Visit](https://www.drupal.org/node/1897420) for further information.

## Configuration

- There is no confuguration as of now.

## Maintainers
- Rajan Kumar - [rajan-kumar](https://www.drupal.org/u/rajan-kumar)
- Tyler Fahey - [twfahey](https://www.drupal.org/u/twfahey)
